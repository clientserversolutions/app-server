const routes = require('express').Router();
const albumModel = require('../models/album')
const pairModel = require('../models/pair')
const checkAuth = require('../middleware/check-auth')
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })
const { v4: uuidv4 } = require('uuid')
const fs = require('fs');
const path = require('path');

routes.post('/', checkAuth, upload.fields([{ name: 'photos' }, { name: 'videos' }]), (req, res) => {

    let { userId } = req.userData

    let { albumName, pairNames } = req.body

    let { videos, photos } = req.files

    let albumId = uuidv4()


    const createAssetsDir = (fileName, assetType) => {
        if (!fs.existsSync(path.join(__dirname, `../../assets/${assetType}/${fileName.slice(0, 2)}/${fileName.slice(2, 4)}/`))) {
            fs.mkdirSync(path.join(__dirname, `../../assets/${assetType}/${fileName.slice(0, 2)}/${fileName.slice(2, 4)}/`), { recursive: true }, (err) => {
                if (err)
                    console.log(err)
            });
        }
    }

    const moveFile = (temporaryFilePath, fileName, assetType) => {
        fs.copyFile(temporaryFilePath, path.join(__dirname, `../../assets/${assetType}/${fileName.slice(0, 2)}/${fileName.slice(2, 4)}/${fileName}`), (err) => {
            if (err) throw err;
            console.log('file copied')

            fs.unlink(temporaryFilePath, function (err) {
                if (err) throw err;
                console.log('file deleted');
            });
        });
    }

    albumModel.createAlbum(albumId, albumName)
        .then(result => {
            console.log(result)
        })
        .catch(error => {
            console.log(error)
        })

    for (let index = 0; index < photos.length; index++) {

        let imagePath = path.join(__dirname, `../../${photos[index].path}`)
        let videoPath = path.join(__dirname, `../../${videos[index].path}`)

        let imageName = uuidv4()
        let videoName = uuidv4()
        let pairName = pairNames[index]

        createAssetsDir(imageName, 'photos')
        createAssetsDir(videoName, 'videos')

        moveFile(imagePath, imageName, 'photos')
        moveFile(videoPath, videoName, 'videos')


        pairModel.createPair(imageName, videoName, pairName)
            .then(result => {
                console.log(result)
                albumModel.addAlbumPair(albumId, imageName)
                    .then(result => {
                        console.log(result)
                    })
                    .catch(error => {
                        console.log(error)
                    })
            })
            .catch(error => {
                console.log(error)
            })
    }

    albumModel.addUserAlbum(userId, albumId)
        .then(result => {
            console.log(result)
            return res.status(200).json({
                message: "Album created",
            });
        })
        .catch(error => {
            console.log(error)
        })

})


routes.post('/:id', checkAuth, (req, res) => {
    const albumId = req.params.id

    let { userId } = req.userData

    albumModel.addUserAlbum(userId, albumId)
        .then(result => {
            console.log(result)
            return res.status(200).json({
                message: "Album added",
            });
        })
        .catch(error => {
            return res.status(500).json(error)
        })

});

routes.get('/', checkAuth, (req, res) => {

    let { userId } = req.userData

    albumModel.getUserAlbums(userId)
        .then(result => {
            return res.status(200).json(result)
        })
        .catch(error => {
            return res.status(400).json(error)
        })
});

routes.delete('/', checkAuth, (req, res) => {

    let { userId } = req.userData
    let { albumId } = req.body

    albumModel.deleteUserAlbum(userId, albumId)
        .then(result => {
            return res.status(200).json(result)
        })
        .catch(error => {
            return res.status(400).json(error)
        })
});

module.exports = routes;