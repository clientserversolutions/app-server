const routes = require('express').Router();
const pairModel = require('../models/pair')
const fs = require('fs');
const path = require('path');
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })
const { v4: uuidv4 } = require('uuid')
const checkAuth = require('../middleware/check-auth')


routes.get('/', checkAuth, (req, res) => {

    let { userId } = req.userData

    pairModel.getUserPairs(userId)
        .then(images => {
            return res.status(200).json(images)
        })
        .catch(error => {
            return res.status(400).json(error)
        })
});

routes.get('/:id', (req, res) => {
    const imageId = req.params.id

    let imagePath = path.join(__dirname, `../../assets/photos/${imageId.slice(0, 2)}/${imageId.slice(2, 4)}/${imageId}`)

    if (!fs.existsSync(imagePath)) {
        return res.status(404).end("Not found");
    }

    res.sendFile(imagePath)
});

routes.post('/', checkAuth, upload.fields([{ name: 'image' }, { name: 'video' }]), (req, res) => {

    let { pairName } = req.body

    let { userId } = req.userData

    let imagePath = path.join(__dirname, `../../${req.files.image[0].path}`)
    let videoPath = path.join(__dirname, `../../${req.files.video[0].path}`)

    if (!fs.existsSync(imagePath) || !fs.existsSync(videoPath)) {
        return res.status(500).end("Error while uploading");
    }

    let imageName = uuidv4()
    let videoName = uuidv4()

    const createAssetsDir = (fileName, assetType) => {
        if (!fs.existsSync(path.join(__dirname, `../../assets/${assetType}/${fileName.slice(0, 2)}/${fileName.slice(2, 4)}/`))) {
            fs.mkdirSync(path.join(__dirname, `../../assets/${assetType}/${fileName.slice(0, 2)}/${fileName.slice(2, 4)}/`), { recursive: true }, (err) => {
                if (err)
                    console.log(err)
            });
        }
    }
    createAssetsDir(imageName, 'photos')
    createAssetsDir(videoName, 'videos')


    const moveFile = (temporaryFilePath, fileName, assetType) => {
        fs.copyFile(temporaryFilePath, path.join(__dirname, `../../assets/${assetType}/${fileName.slice(0, 2)}/${fileName.slice(2, 4)}/${fileName}`), (err) => {
            if (err) throw err;
            console.log('file copied')

            fs.unlink(temporaryFilePath, function (err) {
                if (err) throw err;
                console.log('file deleted');
            });
        });
    }

    moveFile(imagePath, imageName, 'photos')
    moveFile(videoPath, videoName, 'videos')

    pairModel.createPair(imageName, videoName, pairName)
        .then(result => {
            pairModel.addUserPair(userId, imageName)
                .then(result => {
                    return res.status(200).json({
                        message: "Pair created",
                    });
                })
                .catch(error => {
                    return res.status(400).json(error)
                })
        })
        .catch(error => {
            return res.status(400).json(error)
        })
})

routes.delete('/', checkAuth, (req, res) => {

    let { userId } = req.userData
    let { imageId } = req.body

    pairModel.deleteUserPair(userId, imageId)
        .then(result => {
            return res.status(200).json(result)
        })
        .catch(error => {
            return res.status(400).json(error)
        })
});

module.exports = routes;