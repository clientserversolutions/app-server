const routes = require('express').Router();
const fs = require('fs');

routes.get('/stream/:email/:videoId', function (req, res) {
    //var { token, email, videoId } = req.body;           //Проверить токен
    const { token, email, videoId } = req.params
    console.log('Stream ' + videoId + ' requested');

    let path = `./assets/videos/${videoId.slice(0, 2)}/${videoId.slice(2, 4)}/${videoId}`;

    if (!fs.existsSync(path)) {
        return res.status(404).end("Not found");
    }

    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range
    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)
        const end = parts[1]
            ? parseInt(parts[1], 10)
            : fileSize - 1
        const chunksize = (end - start) + 1
        const file = fs.createReadStream(path, { start, end })
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/mp4',
        }
        res.writeHead(206, head);
        file.pipe(res);
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
    }
});

module.exports = routes;