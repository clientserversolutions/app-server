const routes = require('express').Router();
const userModel = require('../models/user');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const checkAuth = require("../middleware/check-auth");


routes.post('/signup', (req, res) => {
    const { email, password } = req.body;
    userModel.findByEmail(email)
        .then(result => {
            if (result.length > 0) {
                return res.status(409).json({
                    message: 'User already exists'
                })
            }
            else {
                bcrypt.hash(password, 10, (error, hash) => {
                    if (error) {
                        return res.status(500).json({
                            error
                        });
                    } else {
                        userModel.createUser(email, hash)
                            .then(result => {
                                return res.status(200).json({email: result})
                            })
                            .catch(error => { return res.status(500).json(error) })
                    }
                })
            }
        })
        .catch(error => {
            return res.status(500).json(error)
        })
});


routes.post("/signin", (req, res, next) => {
    const { email, password } = req.body;

    userModel.findByEmail(email)
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: 'Incorrect login or password'
                })
            }
            bcrypt.compare(password, user[0].password_hash, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'Auth failed'
                    });
                }
                if (result) {
                    const token = jwt.sign(
                        {
                            userId: user[0].id_user,
                            email,
                        },
                        process.env.JWT_KEY,
                        {
                            expiresIn: "10h"
                        }
                    );
                    return res.status(200).json({
                        message: "Auth successful",
                        token
                    });
                }
                res.status(401).json({
                    message: 'Incorrect login or password'
                });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

routes.get('/profile', checkAuth, (req, res) => {
    res.status(200).json({message: "Authorized"})
})

module.exports = routes;