const Pool = require('pg').Pool;
const { DEFAULT_DB_CONNECTION } = require('../config');
const pool = new Pool(DEFAULT_DB_CONNECTION);

const createUser = (email, passwordHash) => new Promise((resolve, reject) => {
    pool.query('INSERT INTO "User" (email, password_hash) VALUES ($1, $2)', [email, passwordHash], (error, result) => {
        if (error)
            return reject(error);

        resolve(email);
    })
});

const findByEmail = (email) => new Promise((resolve, reject) => {
    pool.query('SELECT * FROM "User" WHERE email = $1', [email])
        .then(result => {
            resolve(result.rows)
        })
        .catch(reject);
});



module.exports = {
    createUser,
    findByEmail,
}