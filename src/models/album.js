const Pool = require('pg').Pool;
const { DEFAULT_DB_CONNECTION } = require('../config');
const pool = new Pool(DEFAULT_DB_CONNECTION);

const createAlbum = (id_album, album_name) => new Promise((resolve, reject) => {
    pool.query('INSERT INTO "Album" (id_album, album_name) VALUES ($1, $2)', [id_album, album_name], (error, result) => {
        if (error)
            return reject(error);

        resolve(id_album);
    })
});

const getUserAlbums = (userId) => new Promise((resolve, reject) => {
    pool.query(`SELECT "Album".id_album, album_name, to_char("date_created", \'DD/MM/YYYY\') as date_created
	FROM "Album" join "User_Album" ON ("Album".id_album = "User_Album".id_album)
	WHERE "User_Album".id_user = $1`, [userId])
        .then(result => {
            resolve(result.rows)
        })
        .catch(reject);
})

const deleteUserAlbum = (userId, albumId) => new Promise((resolve, reject) => {
    pool.query(`DELETE FROM "User_Album" WHERE id_user = $1 AND id_album = $2;`, [userId, albumId])
    .then(result => {
        resolve(albumId)
    })
    .catch(reject)
})

const addUserAlbum = (userId, albumId) => new Promise((resolve, reject) => {
    pool.query('INSERT INTO "User_Album" (id_user, id_album) VALUES ($1, $2)', [userId, albumId], (error, result) => {
        if (error)
            return reject(error);
            
        resolve(albumId);
    })
});

const addAlbumPair = (albumId, imageId) => new Promise((resolve, reject) => {
    pool.query('INSERT INTO "Album_Pair" (id_album, id_image) VALUES ($1, $2)', [albumId, imageId], (error, result) => {
        if (error)
            return reject(error);
            
        resolve(albumId);
    })
});

module.exports = {
    createAlbum,
    getUserAlbums,
    deleteUserAlbum,
    addUserAlbum,
    addAlbumPair,
}