const Pool = require('pg').Pool;
const { DEFAULT_DB_CONNECTION } = require('../config');
const pool = new Pool(DEFAULT_DB_CONNECTION);

const getUserPairs = (userId) => new Promise((resolve, reject) => {

    pool.query(`SELECT "User_Pair".id_image, id_video, pair_name, to_char("date_created", \'DD/MM/YYYY\') as date_created 
    FROM "Pair" JOIN "User_Pair" ON ("Pair".id_image = "User_Pair".id_image)
    WHERE id_user = $1`, [userId])
        .then(result => {
            console.log(result.rows)
            resolve(result.rows)
        })
        .catch(reject);
});

const createPair = (imageId, videoId, pairName) => new Promise((resolve, reject) => {
    pool.query('INSERT INTO "Pair" (id_image, id_video, pair_name) VALUES ($1, $2, $3)', [imageId, videoId, pairName], (error, result) => {
        if (error)
            return reject(error);

        resolve(imageId);
    })
});

const addUserPair = (userId, imageId) => new Promise((resolve, reject) => {
    pool.query('INSERT INTO "User_Pair" (id_user, id_image) VALUES ($1, $2)', [userId, imageId], (error, result) => {
        if (error)
            return reject(error);

        resolve(userId);
    })
});

const deleteUserPair = (userId, imageId) => new Promise((resolve, reject) => {
    pool.query('DELETE FROM "User_Pair" WHERE id_user = $1 AND id_image = $2', [userId, imageId], (error, result) => {
        if (error)
            return reject(error);

        resolve(userId);
    })
});



// const deletePair = (userId, imageId) => new Promise((resolve, reject) => {
//     pool.query(`DELETE FROM "Pair" WHERE email = $1 AND id_image = $2;`, [email, imageId])
//         .then(result => {
//             resolve(imageId)
//         })
//         .catch(reject)
// })

module.exports = {
    getUserPairs,
    createPair,
    addUserPair,
    deleteUserPair,
}