const DEFAULT_APP_PORT = 3333;

const DEFAULT_DB_CONNECTION = {
  user: 'postgres',
  host: 'localhost',
  // database: 'photomarket_db',
  database: 'photomarketDB',
  password: 'root',
  port: 5432,
};

module.exports = {
  DEFAULT_DB_CONNECTION,
  DEFAULT_APP_PORT,
};