const userModel = require('../models/user');

const registerUser = (email, password) => new Promise((resolve, reject) => {

    userModel.findByEmail(email)
        .then(result => {
            console.log(result)
            if (result.length < 1) {

                userModel.createUser(email, password)
                    .then(result => {

                        resolve(result)
                    })
                    .catch(reject)
            }
            else
                reject("user already exists")
        })
        .catch(reject)

});


module.exports = {
    registerUser,
};