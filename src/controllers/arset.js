const arsetModel = require('../models/pair');

const getImages = (username) => new Promise((resolve, reject) => {

    arsetModel.getImages(username)
    .then(images => {
        return resolve(images)
    })
    .catch(reject)

});

module.exports = {
    getImages,
}