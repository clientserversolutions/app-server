const { DEFAULT_APP_PORT } = require('./src/config');
const express = require('express');
const userRoutes = require('./src/routes/user');
const imgRoutes = require('./src/routes/img');
const videoRoutes = require('./src/routes/video');
const albumRoutes = require('./src/routes/album');

const fs = require('fs');
const app = express();
const morgan = require('morgan')

const port = process.env.PORT || DEFAULT_APP_PORT;
const router = express.Router();

app.use(express.urlencoded({ extended: true }));
app.use(express.json())
app.use(morgan('dev'))
var cors = require('cors')

app.use(cors())

router.use('/user', userRoutes);
router.use('/img', imgRoutes);
router.use('/video', videoRoutes);
router.use('/album', albumRoutes);
app.use('/api', router);


app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

app.listen(port, function () {
    console.log(`Server started at port ${port}`);
});
